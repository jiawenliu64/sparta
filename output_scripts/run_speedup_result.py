#!/usr/bin/python3

import sys 
import itertools
import pandas as pd

# input parameters
input_file = sys.argv[1]

if input_file.endswith('.txt'):
	input_file_name = input_file[:-4]

out = input_file_name + '.csv'

# variables
size = 36
execution = ['Chicago-1-mode-HTY+HTA','Chicago-2-mode-HTY+HTA','Chicago-3-mode-HTY+HTA','Chicago-1-mode-COOY+HTA','Chicago-2-mode-COOY+HTA','Chicago-3-mode-COOY+HTA','Chicago-1-mode-COOY+SPA','Chicago-2-mode-COOY+SPA','Chicago-3-mode-COOY+SPA','NIPS-1-mode-HTY+HTA','NIPS-2-mode-HTY+HTA','NIPS-3-mode-HTY+HTA','NIPS-1-mode-COOY+HTA','NIPS-2-mode-COOY+HTA','NIPS-3-mode-COOY+HTA','NIPS-1-mode-COOY+SPA','NIPS-2-mode-COOY+SPA','NIPS-3-mode-COOY+SPA','Uber-2-mode-HTY+HTA','Uber-3-mode-HTY+HTA','Uber-2-mode-COOY+HTA','Uber-3-mode-COOY+HTA','Uber-2-mode-COOY+SPA','Uber-3-mode-COOY+SPA','Vast-2-mode-HTY+HTA','Vast-3-mode-HTY+HTA','Vast-2-mode-COOY+HTA','Vast-3-mode-COOY+HTA','Vast-2-mode-COOY+SPA','Vast-3-mode-COOY+SPA','Uracil-2-mode-HTY+HTA','Uracil-3-mode-HTY+HTA','Uracil-2-mode-COOY+HTA','Uracil-3-mode-COOY+HTA','Uracil-2-mode-COOY+SPA','Uracil-3-mode-COOY+SPA']
input_time = [0] * size
index_time = [0] * size
accumulation_time = [0] * size
writeback_time = [0] * size
output_time = [0] * size
total_time = [0] * size

count_execution = 0

# Parse result file
fi = open(input_file, 'r')
for line in fi:
	line_array = line.rstrip().split(" ")
	if(line_array[0] == '[Input'):
		input_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Index'):
		index_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Accumulation]:'):
		accumulation_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Writeback]:'):
		writeback_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Output'):	
		output_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Total'):	
		total_time[count_execution] = float(line_array[-2])
		count_execution += 1

fi.close()


dataframe = pd.DataFrame({'0.Tensor-Mode-Approach':execution,'1.Input Processing':input_time,'2.Index Search':index_time,'3.Accumulation':accumulation_time,'4.Writeback':writeback_time,'5.Output Sorting':output_time,'6.Total time':total_time})
dataframe.to_csv(out,index=False,sep=',')

