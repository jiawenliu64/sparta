#!/usr/bin/python3

import sys 
import itertools
import pandas as pd

# input parameters
input_file = sys.argv[1]

if input_file.endswith('.txt'):
	input_file_name = input_file[:-4]

out = input_file_name + '.csv'

# variables
size = 15
execution = ['Chicago-1-mode-MemoryMode','Chicago-2-mode-MemoryMode','Chicago-3-mode-MemoryMode','NIPS-1-mode-MemoryMode','NIPS-2-mode-MemoryMode','NIPS-3-mode-MemoryMode','Flickr-1-mode-MemoryMode','Flickr-2-mode-MemoryMode','Flickr-3-mode-MemoryMode','Vast-1-mode-MemoryMode','Vast-2-mode-MemoryMode','Vast-3-mode-MemoryMode','Delicious-2-mode-MemoryMode','Delicious-3-mode-MemoryMode','Nell2-2-mode-MemoryMode']
input_time = [0] * size
index_time = [0] * size
accumulation_time = [0] * size
writeback_time = [0] * size
output_time = [0] * size
total_time = [0] * size

count_execution = 0

# Parse result file
fi = open(input_file, 'r')
for line in fi:
	line_array = line.rstrip().split(" ")
	if(line_array[0] == '[Input'):
		input_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Index'):
		index_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Accumulation]:'):
		accumulation_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Writeback]:'):
		writeback_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Output'):	
		output_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Total'):	
		total_time[count_execution] = float(line_array[-2])
		count_execution += 1

fi.close()


dataframe = pd.DataFrame({'0.Tensor-Mode-Approach':execution,'1.Input Processing':input_time,'2.Index Search':index_time,'3.Accumulation':accumulation_time,'4.Writeback':writeback_time,'5.Output Sorting':output_time,'6.Total time':total_time})
dataframe.to_csv(out,index=False,sep=',')

