#!/usr/bin/python3

import sys 
import itertools
import pandas as pd

# input parameters
input_file = sys.argv[1]

if input_file.endswith('.txt'):
	input_file_name = input_file[:-4]

out = input_file_name + '.csv'

# variables
size = 60
execution = ['Chicago-1-mode-Sparta','Chicago-2-mode-Sparta','Chicago-3-mode-Sparta','NIPS-1-mode-Sparta','NIPS-2-mode-Sparta','NIPS-3-mode-Sparta','Flickr-1-mode-Sparta','Flickr-2-mode-Sparta','Flickr-3-mode-Sparta','Vast-1-mode-Sparta','Vast-2-mode-Sparta','Vast-3-mode-Sparta','Delicious-2-mode-Sparta','Delicious-3-mode-Sparta','Nell2-2-mode-Sparta','Chicago-1-mode-OptaneOnly','Chicago-2-mode-OptaneOnly','Chicago-3-mode-OptaneOnly','NIPS-1-mode-OptaneOnly','NIPS-2-mode-OptaneOnly','NIPS-3-mode-OptaneOnly','Flickr-1-mode-OptaneOnly','Flickr-2-mode-OptaneOnly','Flickr-3-mode-OptaneOnly','Vast-1-mode-OptaneOnly','Vast-2-mode-OptaneOnly','Vast-3-mode-OptaneOnly','Delicious-2-mode-OptaneOnly','Delicious-3-mode-OptaneOnly','Nell2-2-mode-OptaneOnly','Chicago-1-mode-IAL','Chicago-2-mode-IAL','Chicago-3-mode-IAL','NIPS-1-mode-IAL','NIPS-2-mode-IAL','NIPS-3-mode-IAL','Flickr-1-mode-IAL','Flickr-2-mode-IAL','Flickr-3-mode-IAL','Vast-1-mode-IAL','Vast-2-mode-IAL','Vast-3-mode-IAL','Delicious-2-mode-IAL','Delicious-3-mode-IAL','Nell2-2-mode-IAL','Chicago-1-mode-DRAMOnly','Chicago-3-mode-DRAMOnly','NIPS-3-mode-DRAMOnly','Flickr-1-mode-DRAMOnly','Vast-2-mode-DRAMOnly','Vast-3-mode-DRAMOnly','Delicious-3-mode-DRAMOnly','Nell2-2-mode-DRAMOnly','Placement-ALLInDRAM','Placement-X','Placement-Y','Placement-HtY','Placement-HtA','Placement-ZLocal','Placement-Z']
input_time = [0] * size
index_time = [0] * size
accumulation_time = [0] * size
writeback_time = [0] * size
output_time = [0] * size
total_time = [0] * size

count_execution = 0

# Parse result file
fi = open(input_file, 'r')
for line in fi:
	line_array = line.rstrip().split(" ")
	if(line_array[0] == '[Input'):
		input_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Index'):
		index_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Accumulation]:'):
		accumulation_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Writeback]:'):
		writeback_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Output'):	
		output_time[count_execution] = float(line_array[-2])
	if(line_array[0] == '[Total'):	
		total_time[count_execution] = float(line_array[-2])
		count_execution += 1

fi.close()


dataframe = pd.DataFrame({'0.Tensor-Mode-Appraoch':execution,'1.Input Processing':input_time,'2.Index Search':index_time,'3.Accumulation':accumulation_time,'4.Writeback':writeback_time,'5.Output Sorting':output_time,'6.Total time':total_time})
dataframe.to_csv(out,index=False,sep=',')

