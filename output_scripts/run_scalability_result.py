#!/usr/bin/python3

import sys 
import itertools
import pandas as pd

# input parameters
input_file = sys.argv[1]

if input_file.endswith('.txt'):
	input_file_name = input_file[:-4]

out = input_file_name + '.csv'

# variables
size = 15
execution = ['NIPS-1-mode-12Threads','NIPS-1-mode-8Threads','NIPS-1-mode-4Threads','NIPS-1-mode-2Threads','NIPS-1-mode-1Threads','VAST-2-mode-12Threads','VAST-2-mode-8Threads','VAST-2-mode-4Threads','VAST-2-mode-2Threads','VAST-2-mode-1Threads','NIPS-3-mode-12Threads','NIPS-3-mode-8Threads','NIPS-3-mode-4Threads','NIPS-3-mode-2Threads','NIPS-3-mode-1Threads']
total_time = [0] * size

count_execution = 0

# Parse result file
fi = open(input_file, 'r')
for line in fi:
	line_array = line.rstrip().split(" ")
	if(line_array[0] == '[Total'):	
		total_time[count_execution] = float(line_array[-2])
		count_execution += 1


fi.close()

dataframe = pd.DataFrame({'0.Tensor-Mode-Threads':execution,'1.Total time':total_time})
dataframe.to_csv(out,index=False,sep=',')
