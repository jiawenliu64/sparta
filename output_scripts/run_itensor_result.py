#!/usr/bin/python3

import sys 
import itertools
import pandas as pd

# input parameters
input_file = sys.argv[1]

if input_file.endswith('.txt'):
	input_file_name = input_file[:-4]

out = input_file_name + '.csv'

# variables
size = 10
execution = ['SpTC1-2137','SpTC2-2143','SpTC3-2151','SpTC4-2163','SpTC5-2164','SpTC6-2169','SpTC7-2170','SpTC8-2177','SpTC9-2178','SpTC10-2190']
ITensor_time = [0.092324, 0.110849, 0.121655, 0.113367, 0.112934, 0.118311, 0.120065, 0.119959, 0.120091, 0.102425]
total_time = [0] * size

count_execution = 0

# Parse result file
fi = open(input_file, 'r')
for line in fi:
	line_array = line.rstrip().split(" ")
	if(line_array[0] == '[Total'):	
		total_time[count_execution] = float(line_array[-2])
		count_execution += 1
		if(count_execution == 10):
			count_execution = 0

fi.close()

dataframe = pd.DataFrame({'0.ID':execution,'1.Total time':total_time,'2.ITensor time':ITensor_time})
dataframe.to_csv(out,index=False,sep=',')
